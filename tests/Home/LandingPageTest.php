<?php

namespace App\Tests\Home;

use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\Panther\PantherTestCase;

class LandingPageTest extends PantherTestCase
{
    use ReloadDatabaseTrait;

    /**
     * Tests that the navbar on top on the page works:
     * @deprecated Should be tested in Javascript
     */
    public function testNavbar(): void
    {
        // static::bootKernel();
        $client = static::createPantherClient(['browser' => static::FIREFOX]);
        $crawler = $client->request('GET', '/');
        $this->assertSelectorAttributeContains('nav header [aria-controls="navbar"]', 'title', 'Ouvrir le menu');
        $client->executeScript('document.querySelector(\'nav header [aria-controls="navbar"]\').click()');
        try {
            $client->waitForVisibility('#navbar li[aria-controls="menu-main"]', 1);
        } catch(\Facebook\WebDriver\Exception\TimeoutException $e) {
            $this->assertFalse(true, 'The $(\'#navar li[aria-controls="menu-main"]\') should have been visible');
        }
        $this->assertSelectorAttributeContains('nav header [aria-controls="navbar"]', 'title', 'Fermer le menu');
        $client->executeScript('document.querySelector(\'#navbar li[aria-controls="menu-main"\').click()');
        $this->assertSelectorIsVisible('#menu-main');
        $client->executeScript('document.querySelector(\'#navbar li[aria-controls="menu-about"\').click()');
        $this->assertSelectorIsNotVisible('#menu-main');
        $this->assertSelectorIsVisible('#menu-about');
    }
}
