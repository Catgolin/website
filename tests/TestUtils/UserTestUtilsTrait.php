<?php

namespace App\Tests\TestUtils;

use App\DataFixtures\TestUserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;

trait UserTestUtilsTrait {
    protected function getTestUser(int $n = 1): User
    {
        $user = $this->getUser("user_$n");
        if($user === null) {
            // Load fixtures
        }
        return $user;
    }

    protected function getUser(string $username): ?User
    {
        $repository = static::getContainer()->get(UserRepository::class);
        return $repository->findOneByUsername($username);
    }
}
