<?php

namespace App\Tests\ApplicationTest\Note;

use App\Entity\Note;
use App\Entity\User;
use App\Tests\ApplicationTest\User\UserTestsProviderTrait;
use App\Tests\ApplicationTest\PagesProviderTrait;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NoteTest extends WebTestCase
{
    use ReloadDatabaseTrait;
    use UserTestsProviderTrait;
    use PagesProviderTrait;

    protected $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * @dataProvider publicPagesProvider
     */
    public function testNoNoteButtonWhenNotLoggedIn(string $url): void
    {
        $this->client->request('GET', $url);
        $this->assertSelectorNotExists('[aria-controls=take-notes]');
        $this->assertSelectorNotExists('#take-notes');
    }

}
