<?php

namespace App\Tests\ApplicationTest;

trait PagesProviderTrait {

    /**
     * @brief Provides pages accessible by everyone.
     * @return
     * - string $url address of the page
     * - string $title title displayed on the page
     */
    public function publicPagesProvider(): array
    {
        return $this->addFrenchLocaleToList([
            "Landing page (default)" => [
                "/",
                "Bienvenue",
            ],
            "Landing page (English)" => [
                "/en",
                "Hello world!",
            ],
            "About (default)" => [
                "/a-propos",
                "À propos",
            ],
            "About (English)" => [
                "/en/about",
                "About",
            ],
            "Legal mentions (default)" => [
                "/mentions-legales",
                "Mentions légales et conditions d'utilisation.",
            ],
            "Legal mentions (English)" => [
                "/en/legal-mentions",
                "Legal mentions and Terms of Use",
            ],
        ]);
    }

    /**
     * @brief Provides pages accessibles only by guests.
     * @return
     * - string $url address of the page
     * - string $title title displayed on the page
     * - string $redirection address of redirection for authentified user
     */
    public function guestPagesProvider(): array
    {
        return $this->addFrenchLocaleToList([
            "Login (default)" => [
                "/connexion",
                "Connectez-vous",
                "/profil",
            ],
            "Login (English)" => [
                "/en/login",
                "Log in",
                "/en/profile",
            ],
            "Register (default)" => [
                "/inscription",
                "Inscrivez-vous !",
                "/profil",
            ],
            "Register (english)" => [
                "/en/register",
                "Register here",
                "/en/profile",
            ],
        ]);
    }

    /**
     * @brief Provides pages accessible by authenticated users
     * @return
     * - string $url address of the page
     * - string $title title displayed on the page
     * - string $redirection address of redirection for guest users
     */
    public function protectedPagesProvider(): array
    {
        return $this->addFrenchLocaletoList([
            "Profile (default)" => [
                '/profil',
                'user_1',
                '/connexion',
            ],
            "Profile (English)" => [
                '/en/profile',
                'user_1',
                '/en/login',
            ],
            "Notes (default)" => [
                '/profil/notes',
                'Mes notes',
                '/connexion',
            ],
            "Notes (English)" => [
                '/en/profile/notes',
                'My notes',
                '/en/login',
            ],
        ]);
    }

    /**
     * @brief Adds the French locale to the lists of pages.
     */
    protected function addFrenchLocaleToList(array $list): array
    {
        foreach($list as $key => $values) {
            if(preg_match('/\(default\)/', $key)) {
                $name = preg_replace('/\(default\)/', '(French)', $key);
                foreach($values as $value) {
                    if($value[0] === '/') {
                        $value = $this->addFrToUrl($value);
                    }
                    $list[$name][] = $value;
                }
            }
        }
        return $list;
    }

    /**
     * @brief Adds '/fr' at the start of the url
     * @param string $url The url to transform
     * @return string The transformed url
     */
    private function addFrToUrl(string $url): string
    {
        if(strlen($url) < 1 || $url === "/") {
            return '/fr';
        }
        return '/fr'.$url;
    }
}
