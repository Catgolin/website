<?php

namespace App\Tests\ApplicationTest;

use Symfony\Component\DomCrawler\Crawler;

const SITE_CONTACT = 'contact@catgolin.eu';

const LOCALES = [
    "Français",
    "English",
];

trait ApplicationTestAssertionsTrait {

    /**
     * @note Needs the crawler to check the link's href attribute
     * (see assertValidContactInformations)
     */
    public function assertValidStaticInformations(Crawler $crawler): void
    {
        $this->assertValidContactInformations($crawler);
        $this->assertTranslationLinksArePresent();
    }

    /**
     * @note Needs the crawler to check the link's href attribute
     */
    public function assertValidContactInformations(Crawler $crawler): void
    {
        $selector = "footer #site-credits a";
        $link = $crawler->selectLink('<'.SITE_CONTACT.'>')->link();
        $this->assertSelectorTextSame($selector, '<'.SITE_CONTACT.'>');
        $this->assertEquals($link->getUri(), 'mailto:'.SITE_CONTACT);
    }

    public function assertTranslationLinksArePresent(): void
    {
        foreach(LOCALES as $locale) {
            $this->assertTranslationLinkExists($locale);
        }
    }

    public function assertTranslationLinkWorks(Crawler $crawler, string $locale, string $expectedUri): void
    {
        $this->assertTranslationLinkExists($locale);
        $link = $crawler->selectLink($locale)->link();
        $this->assertEquals($link->getUri(), $expectedUri);
    }

    public function assertTranslationLinkExists(string $locale): void
    {
        $selector = "footer #translation-footnav";
        $this->assertSelectorTextContains($selector, $locale);
    }

    protected function assertErrorDisplayed(?string $message = null): void
    {
        $selector = "notification-message.error [slot=content]";
        $this->assertSelectorExists($selector, "There should have been an error notification");
        if($message !== null) {
            $this->assertSelectorTextSame($selector, $message);
        }
    }

    protected function assertMessage(?string $message = null): void
    {
        $selector = "notification-message [slot=content]";
        $this->assertSelectorExists($selector);
        if($message !== null) {
            $this->assertSelectorTextSame($selector, $message);
        }
    }
}
