<?php

namespace App\Tests\ApplicationTest;

use App\Tests\TestUtils\UserTestUtilsTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SmokeTest extends WebTestCase
{
    use ApplicationTestAssertionsTrait;
    use PagesProviderTrait;
    use UserTestUtilsTrait;

    protected $testUser;
    protected $client;

    protected function setUp(): void
    {
        parent::tearDown();
        parent::setUp();
        $this->client = static::createClient();
        $this->testUser = $this->getTestUser();
    }

    /**
     * @dataProvider publicPagesProvider
     * @dataProvider guestPagesProvider
     * @group Smoketest
     */
    public function testPublicPageLoads(string $url, string $title): void
    {
        $crawler = $this->client->request('GET', $url);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextSame("main header h1", $title);
        $this->assertValidStaticInformations($crawler);
    }

    /**
     * @dataProvider guestPagesProvider
     * @group Smoketest
     */
    public function testGuestPageRedirectsWhenAuthentified(string $url, string $title, string $redirection): void
    {
        $this->client->loginUser($this->testUser);
        $crawler = $this->client->request('GET', $url);
        $this->assertResponseRedirects($redirection);
    }

    /**
     * @dataProvider publicPagesProvider
     * @group Smoketest
     */
    public function testPublicPageDoesntRedirect(string $url, string $title): void
    {
        $this->client->loginUser($this->testUser);
        $crawler = $this->client->request('GET', $url);
        $this->assertResponseIsSuccessful();
        $this->assertValidStaticInformations($crawler);
    }

    /**
     * @dataProvider protectedPagesProvider
     * @group Smoketest
     * @group Security
     */
    public function testProtectedPageRedirectsGuest(string $url, string $title, string $redirection): void
    {
        $crawler = $this->client->request('GET', $url);
        $scheme = $this->client->getRequest()->getScheme();
        $this->assertResponseRedirects("$scheme://localhost$redirection");
    }

    /**
     * @dataProvider protectedPagesProvider
     * @group Smoketest
     * @group Security
     */
    public function testProtectedPageLoads(string $url, string $title): void
    {
        $this->client->loginUser($this->testUser);
        $crawler = $this->client->request('GET', $url);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextSame("main header h1", $title);
        $this->assertValidStaticInformations($crawler);
    }
}
