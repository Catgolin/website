<?php

namespace App\Tests\ApplicationTest\User;

use App\Entity\User;
use App\Tests\TestUtils\UserTestUtilsTrait;

trait UserTestsProviderTrait
{
    use UserTestUtilsTrait;

    public function userProvider(): array
    {
        $users = [];
        for($i = 1; $i <= 2; $i++) {
            $username = "user_$i";
            $users[$username] = [$this->getUser($username)];
        }
        return $users;
    }

    public function userFixturesInformationsProvider(): array
    {
        $users = [];
        for($i = 1; $i <= 2; $i++) {
            $username = "user_$i";
            $users[$username] = [
                $this->getUser($username),
                "v3ry_s4f3_p4ssw0rd_$i",
            ];
        }
        return $users;
    }

    public function newUserInformationsProvider(): array
    {
        return $this->createUserObjects([
            "Test" => [
                "username" => "Test",
                "email" => "test@catgolin.eu",
                "password" => "v3ry s4f3 p4ssw0rd",
            ],
        ]);
    }

    protected function createUserObjects(array $data): array
    {
        $res = [];
        foreach($data as $label => $userInfos) {
            $user = new User();
            if(isset($userInfos['username'])) {
                $user->setUsername($userInfos['username']);
                unset($userInfos['username']);
            }
            if(isset($userInfos['email'])) {
                $user->setEmail($userInfos['email']);
                unset($userInfos['email']);
            }
            $res[$label] = \array_merge([
                $user,
            ], $userInfos);
        }
        return $res;
    }
}
