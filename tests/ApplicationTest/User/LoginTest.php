<?php

namespace App\Tests\ApplicationTest\User;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginTest extends WebTestCase
{
    use UserAssertionsTrait;
    use UserTestsProviderTrait;

    protected $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * @dataProvider userFixturesInformationsProvider
     */
    public function testLoginFromPage(User $user, string $password): void
    {
        $this->client->request('GET', '/en/login');
        $this->assertResponseIsSuccessful();
        $this->client->submitForm('login', [
            '_username' => $user->getUsername(),
            '_password' => $password,
        ]);
        $this->assertResponseRedirects('http://localhost/en/profile');
        $this->client->followredirect();
        $this->assertSelectorExists('nav header [href="/logout"]');
        $this->assertSelectorNotExists('nav header [aria-controls="login-form"]');
        // $this->assertMessage("Welcome, $user->getUsername()");
    }

    /**
     * @dataProvider userFixturesInformationsProvider
     */
    public function testErrorBadUsername(User $user, string $password): void
    {
        $username = $user->getUsername();
        $this->client->request('GET', '/connexion');
        $this->client->submitForm('login', [
            '_username' => "$username modified",
            '_password' => $password,
        ]);
        $this->assertResponseRedirects('http://localhost/connexion');
        $this->client->followRedirect();
        $this->assertErrorDisplayed('Identifiants invalides.');
    }

    /**
     * @dataProvider userFixturesInformationsProvider
     */
    public function testErrorBadPasswod(User $user, string $password): void
    {
        $this->client->request('GET', '/connexion');
        $this->client->submitForm('login', [
            '_username' => $user->getUsername(),
            '_password' => "modified $password",
        ]);
        $this->assertResponseRedirects('http://localhost/connexion');
        $this->client->followRedirect();
        $this->assertErrorDisplayed('Identifiants invalides.');
    }

    /**
     * @dataProvider userProvider
     */
    public function testLogout(User $user): void
    {
        $this->client->loginUser($user);
        $this->client->request('GET', '/logout');
        $this->assertResponseRedirects('http://localhost/');
        $this->client->followRedirect();
        $this->assertSelectorNotExists('nav header [href="/logout"]');
        $this->assertSelectorExists('nav header [aria-controls=login-form]');
    }
}
