<?php

namespace App\Tests\ApplicationTest\User;

use App\Entity\User;
use App\Tests\TestUtils\UserTestUtilsTrait;
use Faker\Factory;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfileTest extends WebTestCase
{
    use RefreshDatabaseTrait;
    use UserAssertionsTrait;
    use UserTestsProviderTrait;
    use UserTestUtilsTrait;

    protected $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * @dataProvider userProvider
     */
    public function testUserInformationsShown(User $user): void
    {
        $user = $this->getUser($user->getUsername());
        $this->client->loginUser($user);
        $this->client->request('GET', '/profil');
        $this->assertResponseIsSuccessful();
        $this->assertInputValueSame("profile_form[username]", $user->getUsername());
        $this->assertInputValueSame("profile_form[email]", $user->getEmail());
        $this->assertSelectorTextContains('main [aria-controls="update-password"]', 'Modifier mon mot de passe');
    }

    /**
     * @dataProvider userProvider
     */
    public function testUpdateUserInformations(User $user): void
    {
        $faker = Factory::create();
        $user = $this->getUser($user->getUsername());
        $this->client->loginUser($user);
        $username = $faker->username();
        $email = $faker->email();
        $this->client->request('GET', '/profil');
        $this->client->submitForm('update-profile', [
            'profile_form[username]' => $username,
            'profile_form[email]' => $email,
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertMessage('Votre profil a été mis à jour.');
        $user->setUsername($username);
        $user->setEmail($email);
        $this->testUserInformationsShown($user);
    }
}
