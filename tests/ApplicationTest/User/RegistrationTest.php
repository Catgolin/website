<?php

namespace App\Tests\ApplicationTest\User;

use App\Entity\User;
use App\Tests\ApplicationTest\ApplicationTestAssertionsTrait;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationTest extends WebTestCase
{
    use ApplicationTestAssertionsTrait;
    use ReloadDatabaseTrait;
    use UserAssertionsTrait;
    use UserTestsProviderTrait;

    protected $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * @dataProvider newUserInformationsProvider
     */
    public function testUserRegistersWithInformations(User $user, string $password): void
    {
        $crawler = $this->client->request('GET', '/en/register');
        $this->assertResponseIsSuccessful();
        $this->client->submitForm('register', [
            'registration_form[username]' => $user->getUsername(),
            'registration_form[password][plain]' => $password,
            'registration_form[password][confirmation]' => $password,
            'registration_form[email]' => $user->getEmail(),
            'registration_form[agreeTerms]' => true,
        ]);
        $this->assertResponseRedirects('/en/profile');
        $this->assertUserCreated($user);
    }

    /**
     * @dataProvider newUserInformationsProvider
     */
    public function testErrorIfNoEmail(User $user, string $password): void
    {
        $crawler = $this->client->request('GET', '/en/register');
        $this->assertResponseIsSuccessful();
        $this->client->submitForm('register', [
            'registration_form[username]' => $user->getUsername(),
            'registration_form[password][plain]' => $password,
            'registration_form[password][confirmation]' => $password,
            'registration_form[agreeTerms]' => true,
        ]);
        $this->assertUserNotCreated($user);
        $this->assertErrorDisplayed();
    }

    /**
     * @dataProvider newUserInformationsProvider
     */
    public function testErrorIfNoUsername(User $user, string $password): void
    {
        $crawler = $this->client->request('GET', '/en/register');
        $this->assertResponseIsSuccessful();
        $this->client->submitForm('register', [
            'registration_form[password][plain]' => $password,
            'registration_form[password][confirmation]' => $password,
            'registration_form[email]' => $user->getEmail(),
            'registration_form[agreeTerms]' => true,
        ]);
        $this->assertUserNotCreated($user);
        $this->assertErrorDisplayed();
    }

    /**
     * @dataProvider newUserInformationsProvider
     */
    public function testErrorIfNoPassword(User $user): void
    {
        $crawler = $this->client->request('GET', '/en/register');
        $this->assertResponseIsSuccessful();
        $this->client->submitForm('register', [
            'registration_form[username]' => $user->getUsername(),
            'registration_form[email]' => $user->getEmail(),
            'registration_form[agreeTerms]' => true,
        ]);
        $this->assertUserNotCreated($user);
        $this->assertErrorDisplayed();
    }

    /**
     * @dataProvider newUserInformationsProvider
     */
    public function testErrorIfPasswordsDontMatch(User $user, string $password): void
    {
        $crawler = $this->client->request('GET', '/en/register');
        $this->assertResponseIsSuccessful();
        $this->client->submitForm('register', [
            'registration_form[username]' => $user->getUsername(),
            'registration_form[password][plain]' => $password,
            'registration_form[password][confirmation]' => \md5(\rand()),
            'registration_form[email]' => $user->getEmail(),
            'registration_form[agreeTerms]' => true,
        ]);
        $this->assertUserNotCreated($user);
        $this->assertErrorDisplayed();
    }

    /**
     * @dataProvider newUserInformationsProvider
     */
    public function testLoginAfterRegistration(User $user, string $password): void
    {
        $this->testUserRegistersWithInformations($user, $password);
        $this->client->request('GET', '/logout');
        $this->client->request('GET', '/connexion');
        $this->client->submitForm('login', [
            '_username' => $user->getUSername(),
            '_password' => $password,
        ]);
        $this->assertResponseRedirects('http://localhost/profil');
    }
}
