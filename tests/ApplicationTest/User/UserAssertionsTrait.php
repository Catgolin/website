<?php

namespace App\Tests\ApplicationTest\User;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\ApplicationTest\ApplicationTestAssertionsTrait;

trait UserAssertionsTrait
{

    use ApplicationTestAssertionsTrait;

    public function assertUserCreated(User $expected): void
    {
        $repository = $this->getRepository();
        $users = $repository->findByUsername($expected->getUsername());
        $this->assertCount(1, $users);
        $user = $repository->findOneByUsername($expected->getUsername());
        $this->assertEquals($expected->getEmail(), $user->getEmail());
    }

    public function assertUserNotCreated(User $expected): void
    {
        $repository = $this->getRepository();
        $users = $repository->findByUsername($expected->getUsername());
        $this->assertCount(0, $users, $expected->getUsername() . " is in the database");
    }

    public function assertUserAuthenticated(User $user): void
    {
        $selector = 'nav header a[href="/profil"]';
        $this->assertSelectorExists($selector);
        $this->assertSelectorTextSame($selector, $user->getUsername());
    }

    protected function getRepository(): UserRepository
    {
        return static::getContainer()->get(UserRepository::class);
    }
}
