<?php

namespace App\Tests\EndToEndTest\User;

use App\Entity\User;
use App\Tests\ApplicationTest\User\UserTestsProviderTrait;
use App\Tests\ApplicationTest\User\UserAssertionsTrait;
use Symfony\Component\Panther\PantherTestCase;

class LoginTest extends PantherTestCase
{
    use UserTestsProviderTrait;
    use UserAssertionsTrait;

    protected $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createPantherClient(['browser' => static::FIREFOX]);
    }

    public function tearDown(): void
    {
        $this->client->request('GET', '/logout');
        parent::tearDown();
    }

    /**
     * @dataProvider userFixturesInformationsProvider
     */
    public function testLoginModal(User $user, string $password): void
    {
        $url = "/";
        $this->client->request('GET', $url);
        $this->openModal();
        $this->client->submitForm('login', [
            '_username' => $user->getUsername(),
            '_password' => $password,
        ]);
        $this->assertUserAuthenticated($user);
    }

    /**
     * @dataProvider userFixturesInformationsProvider
     */
    public function testErrorLoginModal(User $user, string $password): void
    {
        $url = "/";
        $this->client->request('GET', $url);
        $this->openModal();
        $this->client->submitForm('login', [
            '_username' => $user->getUsername(),
            '_password' => "Bad $password",
        ]);
        $this->assertRedirected('/connexion');
        $this->assertErrorDisplayed('Identifiants invalides.');
        // Should log in and return to previous
        $this->client->submitform('login', [
            '_username' => $user->getUsername(),
            '_password' => $password,
        ]);
        $this->assertUserAuthenticated($user);
        // $this->assertRedirected($url);
    }

    protected function openModal(): void
    {
        $btn = 'nav header [aria-controls=login-form]';
        $this->client->executeScript("document.querySelector('$btn').click()");
        $this->client->waitForVisibility('#login-form', 1);
    }

    public function assertRedirected(string $destination): void
    {
        $this->assertSame(
            self::$baseUri . $destination,
            $this->client->getCurrentURL()
        );
    }

}
