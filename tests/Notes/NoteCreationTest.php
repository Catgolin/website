<?php

namespace App\Tests\Notes;

use App\Entity\Note;
use App\Tests\User\LoginTrait;
use App\Tests\User\UserProviderTrait;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\Panther\PantherTestCase;
use Symfony\Component\Panther\Client;

class NoteCreationTest extends PantherTestCase
{
    use LoginTrait;
    use ReloadDatabaseTrait;
    use UserProviderTrait;

    public function setUp(): void
    {
        parent::setUp();
        static::bootKernel();
    }

    /**
     * Tests that there is a button on any page for the user to add a note when the user is authentified
     * and tests that the note is then printed on the page listing notes
     * and can be printed individually
     * @dataProvider userLoginProvider
     */
    public function testCreateAndPrintNote(string $username, string $password): void
    {
        $client = static::createPantherClient(['browser' => static::FIREFOX]);
        $this->loginPanther($client, $username, $password);
        $this->assertSelectorIsVisible('[aria-controls=take-notes]');
        $this->createNote($client, 'Lorem ipsum', 'This is a very important note', 'test');
        $client->request('GET', '/profil/notes');
        $client->waitFor("#note-$username-1");
        // $this->assertSelectorTextContains("#note-$username-1 [data-content=note-title]", 'Lorem ipsum');
        $client->executeScript("document.querySelector('[aria-controls=edit-note-$username-1]').click()");
        $client->waitForVisibility('input#note_form_title[value="Lorem ipsum"]');
        $this->assertInputValueSame('note_form[category]', 'test');
        $this->assertSelectorTextContains('textarea#note_form_content', 'This is a very important note');
    }

    /**
     * Tests that a user can edit his note when authenticated
     * @dataProvider userLoginProvider
     */
    public function testEditNote(string $username, string $password): void
    {
        $newTitle= 'Lorem ipsum dolor';
        $newContent = 'This is a very important note without mistake';
        $client = static::createPantherClient(['browser' => static::FIREFOX]);
        $this->loginPanther($client, $username, $password);
        $this->createNote($client, 'Lorem ipsum', 'This is a very important note but I made an error', 'test');
        $client->request('GET', '/profil/notes');
        $client->waitFor("#note-$username-1");
        $client->executeScript("document.querySelector('[aria-controls=edit-note-$username-1]').click()");
        $client->waitForVisibility("#edit-note-$username-1");
        $client->submitForm('save-note', [
            'note_form[title]' => $newTitle,
            'note_form[category]' => 'random',
            'note_form[content]' => $newContent,
        ]);
        $this->assertSelectorTextContains('notification-message.success', 'Note sauvegardée');
        $client->executeScript("document.querySelector('[aria-controls=edit-note-$username-1]').click()");
        $client->waitForVisibility("input#note_form_title[value='$newTitle']");
        $this->assertInputValueSame('note_form[category]', 'random');
        $this->assertSelectorTextContains('textarea#note_form_content', $newContent);
    }

    /**
     * Tests that a user can't see an other user's notes
     * @dataProvider userLoginProvider
     */
    public function testAccessOthersNote(string $username, string $password): void
    {
        $client = static::createPantherClient(['browser' => static::FIREFOX]);
        $this->loginPanther($client, $username, $password);
        $this->createNote($client, 'What a splendid note', 'The content should not be shown to someone else!');
        $client->request('GET', '/profil/notes');
        $client->waitFor("#note-$username-1");
        $client->executeScript("document.querySelector('[aria-controls=edit-note-$username-1]').click()");
        $client->waitForVisibility('input#note_form_title[value="What a splendid note"]');
        $client->request('GET', '/logout');
        $this->loginPanther($client, $username . '-other', $password);
        $this->createNote($client, 'This is an other note', 'It has nothing to do with the note of the other one');
        $client->request('GET', '/profil/notes');
        $client->waitFor("#note-$username-other-1");
        $client->executeScript("document.querySelector('[aria-controls=edit-note-$username-other-1]').click()");
        $client->waitForVisibility('input#note_form_title[value="This is an other note"]');
        $this->assertSelectorNotExists("#note-$username-1");
    }

    protected function createNote(Client $client, string $title, string $content, string $category = ''): void
    {
        $client->executeScript('if(document.querySelector(\'#take-notes\').getAttribute(\'hidden\')) document.querySelector(\'[aria-controls=take-notes]\').click()');
        $client->submitForm('save-note', [
            'note_form[title]' => $title,
            'note_form[content]' => $content,
            'note_form[category]' => $category,
        ]);
        $this->assertSelectorTextContains('notification-message.success', 'Note sauvegardée !');
        $this->assertSelectorIsNotVisible('#take-notes');
    }

}
