<?php

namespace App\Tests\UnitTest\DataPersister;

use App\DataPersister\NoteDataPersister;
use App\Entity\Note;
use App\Repository\NoteRepository;
use App\Tests\TestUtils\UserTestUtilsTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Security;

class NoterDataPersisterTest extends KernelTestCase
{
    use UserTestUtilsTrait;

    protected $manager;
    protected $repository;
    protected $security;
    protected $persister;

    public function setUp(): void
    {
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = static::getContainer()->get(NoteRepository::class);
        $this->security = $this->createMock(Security::class);
        $this->persister = new NoteDataPersister($this->manager, $this->security, $this->repository);
    }

    public function testSupportsNote(): void
    {
        $note = new Note();
        $this->assertTrue($this->persister->supports($note));
    }

    /**
     * @dataProvider notesProvider
     */
    public function testPersistsNote(Note $note): void
    {
        $user = $this->getTestUser();
        $this->security->method('getUser')
                       ->willReturn($user)
            ;
        $this->persister->persist($note);
        $this->assertNotePersisted($note);
    }

    /**
     * @dataProvider notesProvider
     */
    public function testRemoveNote(Note $note): void
    {
        $this->persister->remove($note);
        $this->assertNoteRemoved($note);
    }

    public function assertNotePersisted(Note $note): void
    {
        $slug = $note->getSlug();
        $this->assertCount(1, $this->repository->findBySlug($slug));
    }

    public function assertNoteRemoved(Note $note): void
    {
        $slug = $note->getSlug();
        $this->assertNull($this->repository->findOneBySlug($slug));
    }

    public function notesProvider(): array
    {
        return [
            "Empty note" => [$this->emptyNoteProvider()],
            // "Complete note" => [$this->completeNoteProvider()],
        ];
    }

    public function emptyNoteProvider(): Note
    {
        $note = new Note();
        return $note;
    }

    public function completeNoteProvider(): Note
    {
        $user = $this->getTestUser();
        $note = new Note();
        $note->setSlug("what-a-note");
        $note->setAuthor($user);
        $note->setTitle("Lorem ipsum");
        $note->setContent("");
        // $note->setCreatedAt();
        // $note->setUpdatedAt();
        $note->setCategory("Test");
        return $note;
    }
}
