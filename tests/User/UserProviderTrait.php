<?php

namespace App\Tests\User;

trait UserProviderTrait
{
    /**
     * @return string[] a username and a password
     */
    public function userLoginProvider(): array
    {
        return [
            'John Doe' => [
                'John',
                'v3ry s4fe pas5w0rd',
                'john@doe.fr',
            ],
        ];
    }

    /**
     * @return string[] a username, a password and an email
     */
    public function userCreateProvider(): array
    {
        return [
            'John Doe' => [
                'John',
                'v3ry s4fe pas5w0rd',
                'john@doe.com',
            ],
        ];
    }
}
