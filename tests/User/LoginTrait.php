<?php

namespace App\Tests\User;

use App\Entity\User;
use Symfony\Component\Panther\Client as PantherClient;

trait LoginTrait {
    /**
     * @brief Registers the user with the panther client
     * @param string $username: The username of the user
     * @param string $password: The raw password of the user
     * @param string $email: The email adress of the user
     */
    public function registerPanther(PantherClient $client, string $username, string $password, string $email): void
    {
        $client->request('GET', '/inscription');
        $client->executeScript('document.querySelector(\'input[name="registration_form[agreeTerms]"\').click()');
        $client->submitForm('register', [
            'registration_form[username]' => $username,
            'registration_form[password][plain]' => $password,
            'registration_form[password][confirmation]' => $password,
            'registration_form[email]' => $email,
        ]);
    }

    /**
     * @brief Logs the user for the given client
     * @param Client $client: The client to log in
     * @param string $username: The name of the user to log
     * @param string $password: The raw password of the user
     */
    public function loginPanther(PantherClient $client, string $username, string $password): void
    {
        $client->request('GET', '/logout');
        // If the user doesn't exist, create it
        $this->registerPanther($client, $username, $password, $username . '@email.com');
        // Get connexion page
        $crawler = $client->request('GET', '/connexion');
        if($crawler->filter('main button#login')->count() === 1) {
            $client->submitForm('login', [
                '_username' => $username,
                '_password' => $password,
            ]);
        }
    }

    public function assertUserAuthentified(PantherClient $client, string $username): void
    {
        $this->assertSelectorTextContains('nav header a[href="/profil"]', $username);
    }
}
