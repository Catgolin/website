<?php

namespace App\Tests\User;

use App\Entity\User;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Component\Panther\Client;
use Symfony\Component\Panther\PantherTestCase;

class ProfileTest extends PantherTestCase
{
    use LoginTrait;
    use ReloadDatabaseTrait;
    use UserProviderTrait;

    public function setUp(): void
    {
        parent::setUp();
        static::bootKernel();
    }

    /**
     * Tests that the user can update his password
     * @dataProvider userLoginProvider
     */
    public function testUpdatePassword(string $username, string $password): void
    {
        $newPassword = 'whAt a b3aut1fu11 p455w0rd!';
        $client = static::createPantherClient(['browser' => static::FIREFOX]);
        $this->loginPanther($client, $username, $password);
        $client->request('GET', '/profil');
        $client->executeScript('document.querySelector(\'[aria-controls="update-password"]\').click()');
        $client->submitForm('confirm-update-password', [
            'update_password_form[old]' => $password,
            'update_password_form[new][plain]' => $newPassword,
            'update_password_form[new][confirmation]' => $newPassword,
            ]
        );
        $this->assertMessage($client, 'Votre mot de passe a été modifié');
        $client->request('GET', '/logout');
        $this->loginPanther($client, $username, $password);
        $this->assertError($client, 'Identifiants invalides');
        $this->loginPanther($client, $username, $newPassword);
        $this->assertUserAuthentified($client, $username);
    }

    /**
     * Tests that the user can't update his password if it didn't enter the right password
     * @dataProvider userLoginProvider
     */
    public function testUpdatePasswordError(string $username, string $password): void
    {
        $client = static::createPantherClient(['browser' => static::FIREFOX]);
        $this->loginPanther($client, $username, $password);
        $client->request('GET', '/profil');
        $client->executeScript('document.querySelector(\'[aria-controls="update-password"]\').click()');
        $client->submitForm('confirm-update-password', [
            'update_password_form[old]' => $password . ' but not the right password',
            'update_password_form[new][plain]' => $password,
            'update_password_form[new][confirmation]' => $password,
        ]);
        $this->assertError($client, 'Vous n\'avez pas rentré correctement votre mot de passe.');
    }

    /**
     * Tests that the user can remove its profile
     * @dataProvider userLoginProvider
     */
    public function testRemoveProfile(string $username, string $password): void
    {
        $client = static::createPantherClient(['browser' => static::FIREFOX]);
        $this->loginPanther($client, $username, $password);
        $client->request('GET', '/profil');
        $client->executeScript('document.querySelector(\'[aria-controls="remove-profile"]\').click()');
        $client->submitForm('confirm-remove-profile', [
            'confirmation[password]' => $password,
        ]);
        $this->assertMessage($client, 'Votre profil a été supprimé');
        $client->request('GET', '/connexion');
        $client->submitForm('login', [
            '_username' => $username,
            '_password' => $password,
        ]);
        $this->assertError($client, 'Identifiants invalides');
        $this->registerPanther($client, $username, $password, 'random@email.com');
        $this->loginPanther($client, $username, $password);
        $this->assertUserAuthentified($client, $username);
    }

    protected function assertError(Client $client, string $message, bool $remove = true): void
    {
        $this->assertSelectorIsVisible('notification-message.error');
        $this->assertSelectorTextContains('notification-message.error', $message);
        if($remove) {
            $client->executeScript('document.querySelectorAll(\'notification-message.error\').forEach(e => e.close.click())');
            $this->assertSelectorNotExists('notification-message.error');
        }
    }

    protected function assertMessage(Client $client, string $message, bool $remove = true): void
    {
        $this->assertSelectorIsVisible('notification-message');
        $this->assertSelectorTextContains('notification-message', $message);
        if($remove) {
            $client->executeScript('document.querySelectorAll(\'notification-message\').forEach(e => e.close.click())');
            $this->assertSelectorNotExists('notification-message');
        }
    }
}
