import {
	Controller
} from '@hotwired/stimulus';
import {
	addNotification
} from './notification_controller.js';
import {
	ModalDialog
} from './modal_controller.js';

export default class extends Controller {
	connect() {
	}

	save(event) {
		event.preventDefault();
		event.target.dataset.action = "";
		const formData = Object.fromEntries(new FormData(this.element));
		const data = {};
		for (const field of Object.keys(formData)) {
			const name = field.match(/\[(.*)\]/);
			if (name !== null && name.length === 2) {
				data[name[1]] = formData[field];
			}
		}
		const type = this.element.getAttribute('method') === 'PATCH' ? 'application/merge-patch+json' : 'application/ld+json';
		fetch(this.element.getAttribute('action'), {
			method: this.element.getAttribute('method'),
			body: JSON.stringify(data),
			headers: {
				'Content-Type': type,
			},
		}).then(response => {
			if (response.ok) {
				this.displaySuccessMessage();
				this.dispatch('created');
				document.querySelectorAll('[data-controller=notes]').forEach(
					list => {
						console.log(list);
						this.application.getControllerForElementAndIdentifier(list, 'notes').getNotes();
						this.element.reset();
					}, this
				);
				return;
			}
			// handle error
			addNotification(response.statusText, 'error');
			return;
		}).then(() => {
			event.target.dataset.action = "notepad#save";
		});
	}

	displaySuccessMessage() {
		if (this.element.parentElement.parentElement.nodeName === "MODAL-DIALOG") {
			this.element.parentElement.parentElement.setAttribute('hidden', true);
		}
		addNotification(this.element.getAttribute('data-success'), 'success');
	}
}
