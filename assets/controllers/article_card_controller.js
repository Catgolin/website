import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {
	connect() {
	}
}

class ArticleCard extends HTMLElement {
	get title() {
		return this.querySelector('.card-title').innerHTML;
	}
	set title(title) {
		if(!title || title === "") {
			return;
		}
		this.querySelectorAll('.card-title').forEach(element =>
			element.textContent = title
		);
	}

	get content() {
		return this.querySelector('.card-content').innerHTML;
	}
	set content(text) {
		if(!text || text === "") {
			return;
		}
		text = [...text].splice(0, 500).join('') + '...';
		this.querySelectorAll('.card-content').forEach(element =>
			element.textContent = text
		);
		if (!this.title || this.title === "") {
			this.title = text.split(' ').slice(0, 3).join(' ') + '...';
		}
	}

	get author() {
		return this.querySelector('.card-author').innerHTML;
	}
	set author(text) {
		if(!text || text === "") {
			return;
		}
		this.querySelectorAll('.card-author').forEach(element =>
			element.textContent = text
		);
	}

	get link() {
		return this.querySelector('.card-link').getAttribute('href');
	}
	set link(link) {
		if(!link || link === "") {
			return;
		}
		this.querySelectorAll('.card-link').forEach(element =>
			element.setAttribute('href', link)
		);
		this.querySelectorAll('a.card-title').forEach(element =>
			element.setAttribute('href', link)
		);
	}

	get created() {
		return this.querySelector('.card-created').getAttribute('datetime');
	}
	set created(datetime) {
		this.querySelectorAll('.card-created').forEach(element => {
			element.setAttribute('datetime', datetime);
			element.textContent = datetime;
		});
	}

	constructor() {
		super();
		this.dataset.controller = 'article-card';
		const template = document.getElementById('article-card');
		this.appendChild(template.content.cloneNode(true));
		this.title = this.getAttribute("data-title");
		this.content = this.getAttribute("data-content");
		this.link = this.getAttribute("data-link");
		this.author = this.getAttribute("data-author")
			.replace("Auteur: ", "")
			.replace("Author: ", "")
		;
		this.created = this.getAttribute("data-created")
			.replace("Created: ", "")
			.replace("Date: ", "")
		;
		this.messages = {
			delete: {
				success: template.getAttribute('data-message-delete-success'),
				error: template.getAttribute('data-message-delete-error'),
			},
		};
	}
}

if (customElements.get('article-card') === undefined) {
	customElements.define('article-card', ArticleCard);
}

export {
	ArticleCard
};
