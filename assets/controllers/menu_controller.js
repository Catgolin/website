import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {
	get primary() {
		return this.element.parentElement.querySelector("ul[role='tablist']");
	}

	connect() {
		this.primary.appendChild(this.element.title.cloneNode(true));
	}
}

if (customElements.get('menu-category') === undefined) {
	customElements.define('menu-category',
		class extends HTMLElement {
			static get observedAttributes() {
				return ["id"];
			}

			get title() {
				const li = document.createElement("li");
				const a = this.querySelector("a.menu-title");
				a.setAttribute("href", `#${this.getAttribute("id")}`);
				if(this.hasAttribute("selected")) {
					li.setAttribute("aria-selected", true);
				}
				li.setAttribute("aria-controls", this.getAttribute("id"));
				li.dataset.controller = "tab";
				li.appendChild(a);
				return li;
			}

			constructor() {
				super();
				const root = this.attachShadow({
					mode: 'open'
				});
				const template = document.getElementById("menu-category").content.cloneNode(true);
				this.dataset.controller = "menu";
				root.appendChild(template);
			}

			attributeChangedCallback(name, oldValue, newValue) {}
		}
	);
}
