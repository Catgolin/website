import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {
	get target() {
		return document.getElementById(this.element.getAttribute('aria-controls'));
	}

	connect() {
		this.off();
		this.element.addEventListener("click", this.toggle.bind(this));
	}

	on() {
		this.target.removeAttribute("hidden");
		this.element.setAttribute("aria-expanded", true);
		if (this.element.hasAttribute('data-title-close')) {
			this.element.setAttribute("title", this.element.getAttribute("data-title-close"));
		}
	}

	off() {
		this.target.setAttribute("hidden", true);
		this.element.setAttribute("aria-expanded", false);
		if (this.element.hasAttribute('data-title-open')) {
			this.element.setAttribute("title", this.element.getAttribute("data-title-open"));
		}
	}

	toggle(event) {
		event.preventDefault();
		if (this.target.hasAttribute("hidden")) {
			this.on();
		} else {
			this.off();
		}
	}
}

class TogglerButton extends HTMLElement {
	static get observedAttributes() {
		return ["aria-expanded"];
	}

	constructor() {
		super();
		const root = this.attachShadow({
			mode: 'open',
		});
		const template = document.getElementById("toggler-button").content.cloneNode(true);
		this.rootElement = template.lastElementChild;
		this.dataset.controller = "toggle";
		root.appendChild(template);
	}

	attributeChangedCallback(name, oldValue, newValue) {
		if (name === 'aria-expanded') {
			if (newValue === 'true' || newValue === true) {
				this.rootElement.classList.add('expanded');
			} else {
				this.rootElement.classList.remove('expanded');
			}
		}
	}
}
if (customElements.get('toggler-button') === undefined) {
	customElements.define('toggler-button', TogglerButton);
}
