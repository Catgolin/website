import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {
	get canvas() {
		return this.element.children[0];
	}
	get ctx() {
		return this.canvas.getContext('2d');
	}
	get width() {
		return this.canvas.width;
	}
	get height() {
		return this.canvas.height;
	}
	get ratio() {
		return this.width / this.height;
	}

	connect() {
		this.init();
	}

	init() {
		const planets = [];
		const n = 200;
		const maxWeight = 10;
		for(let i = 0; i < n; i++) {
			const x = Math.random() * this.width - this.width / 2;
			const y = Math.random() * this.height - this.height / 2;
			planets.push(new Planet(Math.random() * maxWeight, x, y, Math.random(), Math.random()));
		}
		window.requestAnimationFrame((time) => this.update.bind(this)(planets, time));
		this.update(planets);
	}

	update(planets, time) {
		if(this.previousTime === undefined || time - this.previousTime > 1000/24) {
			this.previousTime = time;
			this.ctx.fillStyle = "black";
			this.ctx.fillRect(0, 0, this.width, this.height);
			for(let i = 0; i < planets.length; i++) {
				for(let j = i+1; j < planets.length; j++) {
					planets[i].attract(planets[j]);
				}
			}
			planets.forEach(planet => planet.move());
			this.draw(planets);
		}
		window.requestAnimationFrame((time) => this.update.bind(this)(planets, time));
	}

	draw(planets) {
		const maxX = Math.max(...planets.map(p => p.x));
		const minX = Math.min(...planets.map(p => p.x));
		const scaleX = this.width / (maxX - minX);
		const maxY = Math.max(...planets.map(p => p.y));
		const minY = Math.min(...planets.map(p => p.y));
		const scaleY = this.height / (maxY - minY);
		const scale = Math.min(scaleX, scaleY);
		const centerX = planets.map(p => p.x).reduce((a, b) => a+b) / planets.length;
		const centerY = planets.map(p => p.y).reduce((a, b) => a+b) / planets.length;
		planets.forEach(planet => this.drawIndividual(planet, scale, centerX, centerY), this);
	}

	drawIndividual(planet, scale, centerX, centerY) {
		const x = ((planet.x - centerX) * scale) + this.canvas.width / 2;
		const y = ((planet.y - centerY) * scale) + this.canvas.height / 2;
		this.ctx.fillStyle = 'white';
		this.ctx.beginPath();
		this.ctx.arc(x, y, planet.m * scale, 0, 2 * Math.PI);
		this.ctx.closePath();
		this.ctx.fill();
	}
}

const G = 1;
const MAX_DIST = 1000;

class Planet {
	constructor(mass, x, y, vX, vY) {
		this.m = mass;
		this.x = x;
		this.y = y;
		this.vX = vX;
		this.vY = vY;
	}

	distance(other) {
		return Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2);
	}

	uX(other) {
		return (other.x - this.x) / Math.sqrt(this.distance(other));
	}

	uY(other) {
		return (other.y - this.y) / Math.sqrt(this.distance(other));
	}

	attract(other) {
		let d = this.distance(other);
		if(d < Math.max(this.m, other.m)) {
			d = Math.max(this.m, other.m);
		}
		if(d > MAX_DIST) {
			d = MAX_DIST;
		}
		const Fa = G * other.m / d;
		this.vX += this.uX(other) * Fa;
		this.vY += this.uY(other) * Fa;
		const Fb = G * this.m / d;
		other.vX -= this.uX(other) * Fb;
		other.vY -= this.uY(other) * Fb;
	}

	move() {
		this.x += this.vX;
		this.y += this.vY;
	}

	draw(ctx) {
	}
}

if (customElements.get('decoration-quote') === undefined) {
	customElements.define('decoration-quote',
		class extends HTMLElement {
			static get observedAttributes() {
				return [];
			}

			constructor() {
				super();
				this.canvas = document.createElement('canvas');
				this.dataset.controller = "decoration";
				this.appendChild(this.canvas);
				window.onresize = this.resizeCanvas.bind(this);
				this.resizeCanvas();
			}

			resizeCanvas() {
				this.canvas.width = this.canvas.parentElement.clientWidth;
				this.canvas.height = this.canvas.parentElement.offsetHeight;
			}

			attributeChangedCallback(name, oldValue, newValue) {}
		}
	);
}
