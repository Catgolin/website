import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {
	connect() {
		this.element.close.addEventListener('click', this.close.bind(this));
	}

	close(event) {
		event.preventDefault();
		this.element.remove();
	}
}

class NotificationMessage extends HTMLElement {
	static number = 0;

	constructor() {
		super();
		if (this.id === "") {
			this.id = `notification-${this.constructor.number}`;
		}
		this.constructor.number++;
		const root = this.attachShadow({
			mode: 'open',
		});
		const template = document.getElementById('notification-message').content.cloneNode(true);
		this.rootElement = template.lastElementChild;
		this.dataset.controller = 'notification';
		root.appendChild(template);
		this.createCloseButton();
	}

	createCloseButton() {
		this.close = document.createElement('a');
		this.close.setAttribute('slot', 'close-button');
		this.close.setAttribute('href', '#');
		this.close.innerHTML = 'x';
		this.appendChild(this.close);
		this.close.setAttribute('aria-controls', this.id);
	}
}

function defineNotifications() {
	if (customElements.get('notification-message') === undefined) {
		customElements.define('notification-message', NotificationMessage);
	}
}
defineNotifications();

function addNotification(content, className) {
	defineNotifications();
	const notification = new NotificationMessage();
	const container = document.createElement('div');
	container.setAttribute('slot', 'content');
	container.innerHTML = content;
	if (className) {
		notification.classList.add(className);
	}
	notification.appendChild(container);
	document.getElementById('notifications').appendChild(notification);
}

export {
	addNotification
};
