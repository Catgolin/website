import {
	Controller
} from '@hotwired/stimulus';
import {
	addNotification
} from './notification_controller.js';
import './modal_controller.js';

export default class extends Controller {
	connect() {
		this.slug = this.element.id;
		this.uri = `/api/notes/${this.slug}`;
		const deleteModal = new(customElements.get('modal-dialog'))();
		deleteModal.id = `delete-${this.slug}`;
		deleteModal.appendChild(document.getElementById('note-delete').content.cloneNode(true));
		deleteModal.setAttribute('hidden', true);
		this.element.appendChild(deleteModal);
		this.element.querySelector('.note-delete').setAttribute('aria-controls', `delete-${this.slug}`);
		const editModal = document.getElementById('take-notes').cloneNode(true);
		editModal.id = `edit-${this.slug}`;
		editModal.querySelector('[slot=header]').remove();
		editModal.querySelector('form').setAttribute('action', this.uri);
		editModal.querySelector('form').setAttribute('method', 'PATCH');
		this.element.appendChild(editModal);
		this.element.querySelector('.open-note').setAttribute('aria-controls', `edit-${this.slug}`);
		this.hydrate();
	}

	hydrate(event) {
		if (event) {
			event.preventDefault();
		}
		fetch(this.uri, {
				method: 'GET',
				headers: {
					'Accept': 'application/ld+json',
				},
			})
			.then(response => response.json())
			.then(data => {
				if (data.title) {
					this.element.title = data.title;
				}
				this.element.category = data.category;
				this.element.created = data.createdAt;
				this.element.updated = data.updatedAt;
				this.element.content = data.content;
			}).catch(data => {
				console.error(data);
			});
	}

	edit(event) {
		if (event) {
			event.preventDefault();
		}
	}

	delete(event) {
		if (event) {
			event.preventDefault();
			event.target.dataset.action = "";
		}
		fetch(`/api/notes/${this.slug}`, {
			method: 'DELETE',
		}).then(response => {
			if (response.ok) {
				this.element.setAttribute('hidden', true);
				setTimeout((element) => element.remove(), 1000, this.element);
				addNotification(this.element.messages.delete.success);
				document.querySelectorAll('[data-controller=notes]').forEach(
					list => {
						console.log(list);
						this.application.getControllerForElementAndIdentifier(list, 'notes').getNotes();
					}, this
				);
				return;
			}
			console.log(response);
			addNotification(this.element.messages.delete.error + ' : ' + response.statusText, 'error');
			throw new Error('Response was not ok');
		}).catch(() => {
			if (event) {
				event.target.dataset.action = "note#delete";
			}
		});
	}
}

class NoteCard extends HTMLElement {
	get title() {
		return this.querySelector('input#note_form_title').getAttribute('value');
	}
	set title(title) {
		this.querySelectorAll('.note-title').forEach(element => element.innerHTML = title);
		this.querySelectorAll('input#note_form_title').forEach(element => element.setAttribute('value', title));
	}

	get created() {
		return this.querySelector('.note-created').getAttribute('dateTime');
	}
	set created(date) {
		this.querySelector('.note-created').setAttribute('dateTime', date);
		this.querySelector('.note-created').innerHTML = new Date(date).toLocaleString();
	}

	get updated() {
		return this.querySelector('.note-updated').getAttribute('dateTime');
	}
	set updated(date) {
		this.querySelector('.note-updated').setAttribute('dateTime', date);
		this.querySelector('.note-updated').innerHTML = new Date(date).toLocaleString();
	}

	get category() {
		return this.querySelector('.note-category').innerHTML;
	}
	set category(category) {
		this.querySelectorAll('.note-category').forEach(element => element.innerHTML = category);
		this.querySelectorAll('input#note_form_category').forEach(element => element.setAttribute('value', category));
	}

	get content() {
		return this.querySelector('textarea#note_form_content').innerHTML;
	}
	set content(text) {
		this.querySelectorAll('textarea#note_form_content').forEach(element => element.innerHTML = text);
		if (!this.title || this.title === "") {
			this.querySelectorAll('.note-title').forEach(element => element.innerHTML = text.split(' ').slice(0, 2).join(' ') + '...');
		}
	}

	constructor() {
		super();
		this.dataset.controller = 'note';
		const template = document.getElementById('note-card');
		this.appendChild(template.content.cloneNode(true));
		this.messages = {
			delete: {
				success: template.getAttribute('data-message-delete-success'),
				error: template.getAttribute('data-message-delete-error'),
			},
		};
	}
}

if (customElements.get('note-card') === undefined) {
	customElements.define('note-card', NoteCard);
}

export {
	NoteCard
};
