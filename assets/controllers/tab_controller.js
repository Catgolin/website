import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {
	static get togglers() {
		if (this._togglers === undefined) {
			this._togglers = new WeakMap();
		}
		return this._togglers;
	}
	static addToggler(tab, toggler) {
		if (!(this.togglers.has(tab))) {
			this.togglers.set(tab, []);
		}
		const array = this.togglers.get(tab);
		array.push(toggler);
		this.togglers.set(tab, array);
	}

	get tabList() {
		let parent = this.element.parentElement;
		while (!(parent.hasAttribute("role") && parent.getAttribute("role") === "tablist")) {
			parent = parent.parentElement;
		}
		return parent;
	}

	get target() {
		return document.getElementById(this.element.getAttribute("aria-controls"));
	}

	connect() {
		this.constructor.addToggler(this.tabList, this);
		this.target.setAttribute("role", "tabpanel");
		if(this.element.getAttribute("aria-selected") === "true" || this.element.getAttribute("aria-selected" === true)) {
			this.on();
		} else {
			this.off();
		}
		this.element.addEventListener("click", this.toggle.bind(this));
	}

	toggle(event) {
		event.preventDefault();
		this.constructor.togglers.get(this.tabList).forEach(toggler => toggler.off());
		this.target.toggleAttribute("hidden");
		this.element.setAttribute("aria-selected", !this.target.hasAttribute("hidden"));
		if(this.element.parentElement !== this.tabList) {
			this.element.parentElement.setAttribute("aria-selected", this.element.getAttribute("aria-selected"));
		}
	}

	on() {
		this.constructor.togglers.get(this.tabList).forEach(toggler => toggler.off());
		this.target.removeAttribute("hidden");
		this.element.setAttribute("aria-selected", true);
		if(this.element.parentElement !== this.tabList) {
			this.element.parentElement.setAttribute("aria-selected", this.element.getAttribute("aria-selected"));
		}
	}

	off() {
		this.target.setAttribute("hidden", true);
		this.element.setAttribute("aria-selected", false);
		if(this.element.parentElement !== this.tabList) {
			this.element.parentElement.setAttribute("aria-selected", this.element.getAttribute("aria-selected"));
		}
	}
}
