import {
	Controller
} from '@hotwired/stimulus';
import './toggle_controller';

export default class extends Controller {
	connect() {}
}

class ModalDialog extends HTMLElement {
	static get observedAttributes() {
		return ["hidden", "id"];
	}

	constructor() {
		super();
		this.setAttribute('role', 'dialog');
		this.dataset.controller = 'modal';
		this.createCloseButton();
		const root = this.attachShadow({
			mode: 'open',
			delegatefocus: true,
		});
		const template = document.getElementById('modal-dialog').content.cloneNode(true);
		this.shadowRoot.appendChild(template);
	}

	createCloseButton() {
		this.close = new (customElements.get('toggler-button'))();
		this.close.setAttribute('aria-controls', this.id);
		this.close.setAttribute('slot', 'close-button');
		this.appendChild(this.close);
	}

	attributeChangedCallback(name, oldValue, newValue) {
		const removeOnClick = (function (event) {
			for (let parent = event.target; parent !== null; parent = parent.parentElement) {
				if (parent === this) {
					return;
				}
			}
			this.setAttribute('hidden', true);
		}).bind(this);
		if (name === 'id') {
			this.close.setAttribute('aria-controls', newValue);
		} else if (name === 'hidden') {
			if (newValue === null || newValue === 'false' || newValue === false) {
				// When the dialog is opened
				this.close.setAttribute('aria-expanded', true);
				this.setAttribute('aria-disabled', false);
				this.previousFocus = document.activeElement;
				this.close.shadowRoot.querySelector('a').focus();
				window.addEventListener('mousedown', removeOnClick);
			} else if (newValue === 'true' || newValue === true || newValue === '') {
				// Whe the dialog is closed
				this.close.setAttribute('aria-expanded', false);
				this.setAttribute('aria-disabled', true);
				if (this.previousFocus instanceof HTMLElement) {
					this.previousFocus.focus();
					this.previousFocus = undefined;
				}
				window.removeEventListener('mousedown', removeOnClick);
				// Reset focus on what is was previously on
			}
		}
	}
}
if (customElements.get('modal-dialog') === undefined) {
	customElements.define('modal-dialog', ModalDialog);
}

export {
	ModalDialog
};
