import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {

	connect() {
		this.getNotes();
		this.element.dataset.action = "notepad:created->notes#getNotes";
	}

	getNotes(event) {
		if (event) {
			event.preventDefault();
		}
		this.httpRequest = new XMLHttpRequest();
		this.httpRequest.onreadystatechange = this.printNotes.bind(this);
		this.httpRequest.open('GET', '/api/notes');
		this.httpRequest.send();
	}

	printNotes() {
		if (this.httpRequest.readyState === XMLHttpRequest.DONE) {
			if (this.httpRequest.status === 200) {
				this.element.querySelectorAll('.loading').forEach(loading => loading.remove());
				const response = JSON.parse(this.httpRequest.responseText);
				const notes = response["hydra:member"];
				if (notes.length === 0) {
					this.element.innerHTML = `<h3 class="loading">${this.element.getAttribute('message-empty')}</h3>`;
				}
				notes.forEach(note => {
					if(this.element.querySelector("#"+note.slug) === null) {
						const div = new (customElements.get('note-card'))();
						div.id = note.slug;
						this.element.appendChild(div);
					} else {
						const div = this.element.querySelector(`#${note.slug}`);
						this.application.getControllerForElementAndIdentifier(div, 'note').hydrate();
					}
				}, this);
			} else {
				this.displayError(this.httpRequest.responseText);
			}
		} else {
			if (!this.element.querySelector('.loading')) {
				const loading = document.createElement("h2");
				loading.style.textAlign = "center";
				loading.style.gridColumn = "1/-1";
				loading.innerHTML = "···";
				loading.classList.add('loading');
				this.element.appendChild(loading);
			}
		}
	}

	displayError(error) {
		const notification = document.createElement('notification-message');
		const content = document.createElement('div');
		content.setAttribute('slot', 'content');
		content.innetHTML = error;
		notification.classList.add('error');
		notification.appendChild(error);
		document.getElementById('notifications').appendChild(error);
	}
}
