import {
	Controller
} from '@hotwired/stimulus';

export default class extends Controller {
	get elements() {
		return [...this.element.children];
	}

	connect() {
		this.element.classList.add('articles-list');
		this.sortBy('created');
	}

	sortBy(criteria) {
		console.log(criteria);
		console.log(this.elements);
		const sorted = this.elements.sort((a, b) =>
			b.getAttribute(`data-${criteria}`).localeCompare(a.getAttribute(`data-${criteria}`))
		);
		this.element.innerHTML = "";
		sorted.forEach(article => this.element.appendChild(article), this);
	}
}
