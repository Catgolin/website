/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import './styles/layout.css';
import './styles/decoration.css';
import './styles/typography.css';
import './styles/forms.css';
import './styles/cards.css';

// start the Stimulus application
import './bootstrap';

// Reformat LaTeX \textsc{} keywords to css small-caps
const latexTags = [
	"textsc",
	"newpage",
];
const htmlTags = [
	'<span style="font-variant: small-caps;">',
	'',
];
for(const i in latexTags) {
	const htmlCloseTag = htmlTags[i].replaceAll(/\<(\S*)\s?.*\>/g, "</$1>");
	const regexp = new RegExp(`\\\\${latexTags[i]}(\\{(.*)\\})?`, 'gu');
	document.querySelectorAll("p, li").forEach(
		element =>
		element.innerHTML = element.innerHTML.replaceAll(
					regexp,
					`${htmlTags[i]}$2${htmlCloseTag}`
		)
	);
}
