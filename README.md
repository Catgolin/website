English version bellow

# Version française

## Présentation générale
Ceci est le dépôt du code source de mon site personnel - [https://catgolin.eu].

## Informations techniques
Le projet peut être installé en utilisant composer.
### Tests
Pour lancer les tests, lancer `composer test`.

Les tests sont conçus avec phpunit et utilisent les fonctionnalités de Symfony/Panther. Le navigateur Firefox doit être installé.
### Installation
Pour lancer le projet depuis un terminal:
- `composer serve`

Pour compiler les fichiers pour l'environnement de production:
- `composer prepare`

## Contributions
Le code est accessible sous licence GNU/GPL3.

Le but du projet est d'être un site personnel, mais si vous souhaitez contribuer d'une façon ou d'une autre, n'hésitez pas à faire un pull request.



# English version

## General presentation
This is the repository of my personal website's source code - [https://catgolin.eu].

## Technical informations
This project can be installed using composer.
### Tests
To launch tests, run `composer test`.

Tests are concived with phpunit and are using functionalities of Symfony/Panther. The Firefox browser needs to be installed.
### Installation
To launch the project from a terminal
- `composer serve`

To compile files for prod environement
- `composer prepare`

## Contributions
The code is accessible under the GNU/GPL3 licence.

The aim of the project is to be a personal website, but if you want to contribute in one way or another, feel free to open a pull request.