<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NoteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: NoteRepository::class)]
#[UniqueEntity('slug')]
#[ApiResource(
    iri: "http://purl.org/ontology/bibo/Note",
    collectionOperations: [
        'get' => [
            'method' => 'get',
            'normalization_context' => [
                'groups' => ['note:read'],
                "swagger_definition_name" => "list",
            ],
            'security' => 'is_granted("ROLE_USER")',
        ],
        'post' => [
            'method' => 'post',
            'denormalization_context' => [
                'groups' => ['note:create', 'note:write'],
                "swagger_definition_name" => "create",
            ],
            'security' => 'is_granted("ROLE_USER")',
        ],
    ],
    itemOperations: [
        'get' => [
            'method' => 'get',
            'security' => 'is_granted("GET_NOTE", object)',
            'normalization_context' => [
                'groups' => ['note:read'],
                "swagger_definition_name" => "read",
            ],
        ],
        'patch' => [
            'method' => 'patch',
            'security' => 'is_granted("PATCH_NOTE", object)',
            'denormalization_context' => [
                'groups' => ['note:write'],
                "swagger_definition_name" => "update",
            ],
        ],
        'delete' => [
            'security' => 'is_granted("DELETE_NOTE", object)',
            'method' => 'delete',
        ],
    ],
)]
class Note
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(identifier: false)]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[ApiProperty(
        identifier: true,
        iri: "http://purl.org/ontology/bibo/handle",
        attributes: [
            "openapi_context" => [
                "description" => "note-[username]-[number]",
                "example" => "note-john+doe-42",
            ],
        ],
    )]
    #[Groups(['note:create', 'note:read'])]
    private $slug;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'notes')]
    #[ORM\JoinColumn(nullable: false)]
    #[ApiProperty(
        iri: "http://purl.org/ontology/owner",
        attributes: [
            "openapi_context" => [
                "example" => "/api/users/john+doe"
            ],
        ],
    )]
    #[Groups(['note:read'])]
    private $author;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['note:write', 'note:read'])]
    #[ApiProperty(
        iri: "http://purl.org/dc/terms/title",
        attributes: [
            "openapi_context" => [
                "example" => "Lorem ipsum",
            ],
        ],
    )]
    private $title;

    #[ORM\Column(type: 'text')]
    #[Groups(['note:write', 'note:read'])]
    #[ApiProperty(
        iri: "http://www.w3.org/1999/02/22-rdf-syntax-ns#value",
        attributes: [
            "openapi_context" => [
                "example" => "Lorem ipsum dolor\netc.",
            ],
        ],
    )]
    private $content;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['note:read'])]
    #[ApiProperty(
        iri: "http://purl.org/dc/terms/created",
        attributes: [
            "openapi_context" => [
                "example" => "2022-04-14T21:52:24+00:00"
            ],
        ],
    )]
    private $createdAt;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['note:read'])]
    #[ApiProperty(
        iri: "http://purl.org/dc/terms/updated",
        attributes: [
            "openapi_context" => [
                "example" => "2022-04-15T12:13:14+00:00",
            ],
        ],
    )]
    private $updatedAt;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['note:write', 'note:read'])]
    #[ApiProperty(
        attributes: [
            "openapi_context" => [
                "description" => "A tag to help sort the user's notes",
                "example" => "test",
            ],
        ],
    )]
    private $category;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }
}
