<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Note;
use App\Repository\NoteRepository;
use Symfony\Component\Security\Core\Security;

final class NoteCollectionDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    public function __construct(
        private NoteRepository $repository,
        private Security $security
    )
    {}

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Note::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []) : iterable
    {
        $collection = $this->repository->findByAuthor($this->security->getUser());
        foreach($collection as $note) {
            if($this->security->isGranted('GET_NOTE', $note)) {
                yield $note;
            }
        }
    }
}
