<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ConfirmationType;
use App\Form\ProfileFormType;
use App\Form\RegistrationFormType;
use App\Form\UpdatePasswordFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class UserController extends AbstractController
{
    public function __construct(protected EntityManagerInterface $entityManager)
    {
    }

    #[Route('/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): void
    {}

    #[Route(
        path: [
            'fr_FR' => '/inscription',
            'fr' => '/{_locale}/inscription',
            'en' => '/{_locale}/register',
        ],
        name: 'app_register'
    )]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserAuthenticatorInterface $userAuthenticator, AuthenticatorInterface $authenticator): Response
    {
        if($this->getUser() !== null) {
            $this->addFlash('info', 'message.login.already');
            return $this->redirectToRoute('user_profile');
        }
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('password')['plain']->getData()
                )
            );
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            // do anything else you need here, like send an email
            $userAuthenticator->authenticateUser($user, $authenticator, $request);
            $this->addFlash('success', 'message.registration.success');
            return $this->redirectToRoute('user_profile');
        }

        return $this->render('user/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route(
        path: [
            'fr_FR' => '/connexion',
            'fr' => '/{_locale}/connexion',
            'en' => '/{_locale}/login',
        ],
        name: 'app_login'
    )]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $errors = $authenticationUtils->getLAstAuthenticationError();
        if($this->getUser() !== null) {
            $this->addFlash('info', 'message.login.already');
            return $this->redirectToRoute('user_profile');
        }
        return $this->render('user/login.html.twig', [
            'error' => $errors
        ]);
    }

    #[Route(
        path: [
            'fr_FR' => '/profil',
            'fr' => '/{_locale}/profil',
            'en' => '/{_locale}/profile',
        ],
        name: 'user_profile',
    )]
    public function profile(Request $request, UserpasswordHasherInterface $userPasswordHasher, TokenStorageInterface $tokenStorage): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $form = $this->createForm(ProfileFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->addFlash('success', 'message.profile.updated');
        } else if($form->isSubmitted()) {
            $this->entityManager->refresh($user);
            foreach($form->getErrors(true) as $error) {
                $this->addFlash('error', $error->getMessage());
            }
        }
        $passwordForm = $this->changePassword($request, $userPasswordHasher);
        $removeForm = $this->removeProfile($request, $userPasswordHasher, $tokenStorage);
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('user/profile.html.twig', [
            'updateProfileForm' => $form->createView(),
            'updatePasswordForm' => $passwordForm->createView(),
            'confirmationForm' => $removeForm->createView(),
        ]);
    }

    public function loginForm(AuthenticationUtils $authenticationUtils): Response
    {
        $errors = $authenticationUtils->getLAstAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('user/_login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $errors,
        ]);
    }

    protected function changePassword(Request $request, UserPasswordHasherInterface $userPasswordHasher): Form
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $form = $this->createForm(UpdatePasswordFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('old')->getData();
            if($userPasswordHasher->isPasswordValid($user, $password)) {
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $form->get('new')['plain']->getData()
                    )
                );
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $this->addFlash('success', 'message.password.changed');
            } else {
                $this->addFlash('error', 'message.password.invalid');
            }
        } else if($form->isSubmitted()) {
            foreach($form->getErrors(true) as $error) {
                $this->addFlash('error', $error->getMessageTemplate());
            }
        }
        return $form;
    }

    protected function removeProfile(Request $request, UserPasswordHasherInterface $userPasswordHasher, TokenStorageInterface $tokenStorage): Form
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $form = $this->createForm(ConfirmationType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('password')->getData();
            if($userPasswordHasher->isPasswordValid($user, $password)) {
                // Logout user
                if ($this->getUser()->getId() === $user->getId()) {
                    $tokenStorage->setToken();
                }
                // Remove user
                $this->entityManager->remove($user);
                $this->entityManager->flush();
                $this->addFlash('success', 'message.profile.removed');
            } else {
                $this->addFlash('error', 'message.password.invalid');
            }
        } else if($form->isSubmitted()) {
            foreach($form->getErrors(true) as $error) {
                $this->addFlash('error', $error->getMessage());
            }
        }
        return $form;
    }
}
