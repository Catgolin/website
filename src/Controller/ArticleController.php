<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route(
        path: [
            'fr_FR' => '/articles',
            "fr" => "/articles",
            "en" => "/{_locale}/articles",
        ],
        name: "list_articles",
    )]
    public function index(): Response
    {
        $finder = new Finder();
        $finder->files()
               ->name("*.html")
               ->followLinks()
               ->in(__DIR__."/../../public/uploads/articles")
            ;
        $articles = [];
        foreach($finder as $file) {
            $article = new Crawler("<<<'HTML'".$file->getContents()."HTML;");
            $articles[] = [
                "name" => $file->getFilenameWithoutExtension(),
                "title" => $article->filter('title')->first()->text(),
                "author" => $article->filter('#postamble .author')->first()->text(),
                "abstract" => $article->filter('.abstract')->first()->text(),
                "created" => $article->filter('#postamble .date')->first()->text(),
            ];
        }
        return $this->render('article/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    #[Route(
        path: [
            "fr_FR" => '/article/{article}',
            "fr" => "/{_locale}/article/{article}",
            "en" => "/{_locale}/article/{article}",
        ],
        name: 'read_article'
    )]
    public function read(string $article): Response
    {
        $finder = new Finder();
        $finder->files()
               ->followLinks()
               ->name("$article.html")
               ->in(__DIR__."/../../public/uploads/articles")
            ;
        foreach($finder as $file) {
            $article = new Crawler("<<<'HTML'".$file->getContents()."HTML;");
            $name = $file->getFilenameWithoutExtension();
            $title = $article->filter('title')->first()->text();
            $author = $article->filter('meta[name=author]')->first()->attr('content');
            $content = $file->getContents();
            return $this->render('article/article.html.twig', [
                "article" => $content,
                "title" => $title,
                "author" => $author,
            ]);
        }
    }
}
