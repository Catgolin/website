<?php

namespace App\Controller;

use App\DataPersister\NoteDataPersister;
use App\Entity\Note;
use App\Form\NoteFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NoteController extends AbstractController
{
    public function __construct(
        protected NoteDataPersister $dataPersiste,
        private EntityManagerInterface $entityManager
    ) {}

    #[Route(
        path: [
            'fr_FR' => '/profil/notes',
            'fr' => '/{_locale}/profil/notes',
            'en' => '/{_locale}/profile/notes',
        ],
        name: 'app_notes'
    )]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('note/index.html.twig', []);
    }

    public function createNote(Request $request): Response
    {
        $note = new Note();
        $form = $this->createForm(NoteFormType::class, $note);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->dataPersister->persist($note);
            $this->addFlash('success', 'message.note.created');
        } else if($form->isSubmitted()) {
            foreach($form->getErrors(true) as $error) {
                $this->addFlash('error', $error->getMessage());
            }
        }
        return $this->render('note/_form.html.twig', [
            'noteForm' => $form->createView(),
        ]);
    }
}
