<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LandingController extends AbstractController
{
    #[Route(
        path: [
            'fr_FR' => '/',
            'fr' => '/{_locale}',
            'en' => '/{_locale}',
        ],
        name: 'app_home'
    )]
    public function index(): Response
    {
        return $this->render('special/landing.html.twig');
    }

    #[Route(
        path: [
            'fr_FR' => '/a-propos',
            'fr' => '/{_locale}/a-propos',
            'en' => '/{_locale}/about'
        ],
        name: 'app_about'
    )]
    public function about(): Response
    {
        return $this->render('special/about.html.twig');
    }

    #[Route(
        path: [
            'fr_FR' => '/mentions-legales',
            'fr' => '/{_locale}/mentions-legales',
            'en' => '/{_locale}/legal-mentions',
        ],
        name: 'app_legal'
    )]
    public function legal(): Response
    {
        return $this->render('special/legal.html.twig');
    }

    public function legalMentions(): Response
    {
        return $this->render('special/_legal.html.twig');
    }
}
