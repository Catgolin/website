<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, [
                'mapped' => true,
                'label' => 'label.profile.username',
                'help' => 'help.profile.username',
                'attr' => [
                    'placeholder' => 'field.username',
                    'autocomplete' => 'username',
                    'title' => 'help.profile.username',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'constraint.username.not_blank',
                    ])
                ],
            ])
            ->add('email', EmailType::class, [
                'mapped' => true,
                'label' => 'label.profile.email',
                'help' => 'help.profile.email',
                'attr' => [
                    'placeholder' => 'field.profile.email',
                    'autocomplete' => 'email',
                    'title' => 'help.profile.email',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'constraint.email.not_blank',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
