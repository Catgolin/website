<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotCompromisedPassword;

class RegistrationFormType extends ProfileFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'help' => 'help.terms.agree',
                'label' => 'label.terms.agree',
                'attr' => [
                    'title' => 'help.terms.agree',
                ],
                'constraints' => [
                    new IsTrue([
                        'message' => 'contraint.agree_terms',
                    ]),
                ],
            ])
            ->add('password', RepeatedType::class, [
                'invalid_message' => 'constraint.password.should_match',
                'type' => PasswordType::class,
                'mapped' => false,
                'required' => true,
                'first_name' => 'plain',
                'second_name' => 'confirmation',
                'first_options' => [
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'placeholder' => 'field.password',
                        'title' => 'help.password.create',
                    ],
                    'label' => 'label.password.create',
                    'help' => 'help.password.create',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'constraint.password.not_blank',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'constraint.password.min_length',
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                            'maxMessage' => 'constraint.password.max_length',
                        ]),
                        new NotCompromisedPassword([
                            'message' => 'constraint.password.not_compromised',
                        ]),
                    ],
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'field.password.confirm',
                        'autocomplete' => 'off',
                        'title' => 'help.password.confirm',
                    ],
                    'label' => 'label.password.confirm',
                    'help' => 'help.password.confirm',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
