<?php

namespace App\Form;

use App\Entity\Note;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoteFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'mapped' => true,
                'label' => 'label.note.title',
                'help' => 'help.note.title',
                'attr' => [
                    'placeholder' => 'field.note.title',
                    'title' => 'help.note.title',
                ],
            ])
            ->add('content', TextAreaType::class, [
                'mapped' => true,
                'label' => 'label.note.content',
                'help' => 'help.note.content',
                'attr' => [
                    'placeholder' => 'field.note.content',
                    'title' => 'help.note.content',
                ],
            ])
            ->add('category', TextType::class, [
                'mapped' => true,
                'label' => 'label.note.category',
                'help' => 'help.note.category',
                'attr' => [
                    'placeholder' => 'field.note.category',
                    'title' => 'help.note.category',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Note::class,
        ]);
    }
}
