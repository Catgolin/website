<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Note;
use App\Repository\NoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class NoteDataPersister implements DataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security $security,
        private NoteRepository $repository
    ) {
    }

    public function supports($data): bool
    {
        return $data instanceof Note;
    }

    public function persist($data): void
    {
        if($data->getAuthor() === null) {
            $data->setAuthor($this->security->getUser());
        }
        if($data->getSlug() === null) {
            $this->updateSlug($data, 1);
        }
        if($data->getCreatedAt() === null) {
            $data->setCreatedAt(new \DateTimeImmutable());
        }
        if($data->getContent() === null) {
            $data->setContent("");
        }
        $data->setUpdatedAt(new \DateTime());
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }

    private function isSlugDuplicated(Note $data): bool
    {
        return $this->repository->findOneBySlug($data->getSlug()) !== null;
    }

    private function updateSlug(Note $data, int $number): string
    {
        $data->setSlug("note-" . $data->getAuthor()->getUsername() . "-" . $number);
        if(!$this->isSlugDuplicated($data)) {
            return $data->getSlug();
        }
        return $this->updateSlug($data, ++$number);
    }
}
