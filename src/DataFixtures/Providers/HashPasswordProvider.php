<?php

namespace App\DataFixtures\Providers;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * https://yalit.be/blog/2021/08/17/symfony-hautelook-alice-hash-passwords-at-fixtures-load-5-3-and-upwards/
 */
class HashPasswordProvider
{
    public function __construct(private UserPasswordHasherInterface $hasher) {}

    public function hashPassword(string $plainPassword, ?User $user = null): string
    {
        if($user === null) {
            $user = new User();
        }
        return $this->hasher->hashPassword($user, $plainPassword);
    }
}
